import asyncio
import sdp_transform
import json
class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        messageJson = json.loads(data.decode())
        print('Received offer from', addr)
        answer = sdp_transform.parse(messageJson['sdp'])
        # Cambiar los puertos en la respuesta
        answer['media'][0]['port'] = 34543
        answer['media'][1]['port'] = 34543
        print(answer['media'][0]['port'], answer['media'][1]['port'])
        print('Sending answer...')
        answer_modified = sdp_transform.write(answer)

        answerJson = {
            "type": "answer",
            'sdp': answer_modified}
        jsonanswer = json.dumps(answerJson)
        self.transport.sendto(jsonanswer.encode(), addr)


async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(), local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


asyncio.run(main())
